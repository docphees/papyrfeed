# PAPYRFEED
## A PAPERWORK FEEDER

### OVERVIEW
Papyrfeed is Enhancing the functionality of the Paperwork (https://www.openpaper.work/) document management by adding local, serverless mail & mail attachment support and automated document import from one or more path based feeders.

Papyrfeed is meant to be as low-profile as possible, relying as much as possible on building on as few stable, available solutions as possible to allow a robust, fire-and-forget application without too many dependencies. (See below, 'Dependencies')

### DETAILS
Compared to cloud based document solutions, Paperwork is strictly local, ginving you full control over your documents. Lovely. But cloud solutions have a bit of funcitonality that is really nice: Having a way to send documents via mail into your document vault without having to import them, and further automated imports.

Papyrfeed is aiming to add that, but under your full control and as far as possible agnostic of any service providers. It is following the idea of using Paperwork as a local program and so sticks to the idea of not being a background, allways-on service, but runs on your own machine, only importing when you tell it to.

- Currently there are two import solutions ('feeds') being added:
    1. **Automated IMAP Feed**
        Configure an IMAP mailbox and have attachments or mail bodies converted to PDF if possible and added to your Paperwork collection
    2. **Automated Path Feeds**
        Configure one or more paths to being automatically checked for files, which are then converted to PDFs and automatically added to your Paperwork colleciton. The path(s) can be set up as you want - from a simple local drop-off directory to a synced dropbox, nextcloud or network path - allows for flexibly feeding your Paperwork collection.

- Reporting
    Some very low-profile, mainly status/error reporting is being planned, possibly via Mail or a Message window / text messaging / custom report-command. It is meant to simply tell the user about it not-working.


### DEPENDENCIES
As Papyrfeed is in its alpha stage, the current dependencies may be subject to change. Currently a few solutions are tested in parallel for stability and usability:
- Mail and Mail attachment handling
    The brilliant CLI mail solution 'himalaya' is being evaluated as well as the python-native imap-tools module.
- Conversion to PDF
    Currentl Libre-Office based 'unoconv' is being used, as it provides out-of-the-box conversion for many image and document formats. However, other solutions have to be tested and it might be feasible to allow the user to set-up file name extension and/or media type (MIME type) based custom conversion commands.

### PLAN
The plan is to include sender filtering, adding tags per feed and build out autommaeted conversion to PDF.

- Importing to Paperwork
    Import should usually be run before or while launching Paperwork.
    It is being considered to allow starting and using only certain modules, for example to fetch mails and attachments regularly, push them into a synced or web directory, convert them to PDF on some other machine and import them on again another one using Paperworks CLI functionality, while even accessing the Paperwork collection ona again another machine.

- What about a GUI?
    Sure, when Papyrfeed runs from the CLI, a simple GUI may be built to mainly allow configuration

Additionally a remote access solution could be considered:
- Remote Access
    A very simple, possibly mail based request system could be implemented to search a running Paperwork installation. Security could be handled via PGP encryption. This would provide a simple one/two-stage document retrieval: First step would return a keyword based search result list (including document abstracts, date, size, tags) and quick-answer links, second step would be a link-based document selection mail, which would return the requested document(s).
    A simplification could be added: If only one document is found for a search request and its size is below an optional max size, it would be directly returned, without the first result list step.
    Other remote control schemes could be evaluated, including result return not via mail, but for example via copy-to-path, allowing sync-systems like dropbox, nextcloud etc. to push a requested document to a device.

Security
- Paperwork is not being planned to be a huge, multiuser document management solution, but more for personal/family/small group use. Papyrfeed is therefore not thought of as a high-security service, but as a simple, local extension of the current Paperwork functionality. Security is being thought of, but in the end it will not be better then the general security that the account it is running on can provide by its own.


