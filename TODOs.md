# PAPIERFREI
Dokumentenmanagement für Rinks

## ANSATZ
- Paperwork2 zum Management
- Verwenden von Himalaya (himalaya -o json, himalaya attachments <id>) zur Extraktion von Attachments (https://pimalaya.org/himalaya/cli/usage/attachments/download.html)
- fetchmal und ein parser (suchen) (mutt?)

## FORTSCHRITT
- Paperwork ist installiert und im Prinzip fertig
- Paperwork kann per cli bedient werden um Dokumente hinzuzufügen
- himalaya ist konfiguriert und funktioniert
  himalaya ist ein CLI programm und funktioniert problemlos aus der Kommandozeile
  Daten können in **json** (hinzufügen von -o json) ausgegeben werden!
    - [x] parsen funktioniert noch nicht automatisiert, sollte aber gut gehen
    - [ ] umwandeln in pdf
    - [ ] speichern von mails ohne attachment als pdf
    - [ ] Deployment (cronjob?)

## Konfigurationen

### himalaya Test-Konfiguration (~/.config/himalaya/config.toml)
		["scan2023@rinks.eu"]
		backend = "imap"
		email = "scan2023@rinks.eu"
		downloads-dir = "~/tmp/himalaya-test/"
		default = true
		display-name = "scan2023@rinks.eu"
		imap-host = "web274.dogado.net"
		imap-port = 143
		imap-starttls = true
		imap-login = "scan2023@rinks.eu"
		imap-auth = "passwd"
		imap-passwd = { "raw" = "nintendo acutely taps smith flame" }
		sender = "smtp"
		smtp-host = "web274.dogado.net"
		smtp-port = 587
		smtp-starttls = true
		smtp-login = "scan2023@rinks.eu"
		smtp-auth = "passwd"
		smtp-passwd = { "raw" = "nintendo acutely taps smith flame"}

		["scan2023@rinks.eu".pgp]
		backend = "none"

