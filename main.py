# PAPYRFEED - A Mail Attachment Extractor for Paperwork

# import email
# from imapclient import IMAPClient
# from imap_tools import MailBox
# import json

import imap_tools                   # fetching mail and attachments
import smtplib                      # sending reports (and request results if implemented)
import sys
import os.path
import subprocess                   # For command execution
import json
import config
import configparser                 # switch to configParser
import platformdirs as pdirs        # instead of appdirs

"""
Papyrfeed is a small python application that extracts attachments or mail bodies from
a dedicated mailbox, converts them to PDF and imports them to your paperwork collection and then launches Paperwork.

[x] If a message contains at least one attachment, all attachments are saved to self.download_path.
[x] If a message does not contain any attachments, the html-body (if available) or the text-body will be saved to
self.download_path.
[ ] TODO: Check for PATHs availability or create them
[ ] TODO: Check for network availability before fetching mail
[ ] TODO: Check for converter availability before converting
[ ] TODO: Check for Paperwork availability
[ ] TODO: All files in self.download_path are converted to PDF format if at all possible.
    [ ] TODO: Find good conversion program (dependency)
        [x] Use "unoconv -f pdf -eSelectPdfVersion=1 your-file.odt" (from libreoffice) to convert next to anything
            https://ask.libreoffice.org/t/convert-files-to-pdf-a-on-command-line-headless-mode/821
        [ ] Check PDF-Printer?
[ ] TODO: All PDF will be imported into the Paperwork collection.
[ ] TODO: An error-message will be returned, if an attachment/mail-body cannot be converted into PDF format.
[ ] TODO: An error-message will be returned, if a document cannot be imported into Paperwork.
[ ] TODO: Add some progress report, possibly in a GUI (simplegui?) and 
[ ] TODO: launch Paperwork when done with conversions (toggle off via argument)...
[ ] TODO: LOGGING!! (also: config-file)
[ ] TODO: REPORTING! (also: config-file)
[ ] TODO: Nice simple GUI with simplegui?
"""

_APPNAME = "papyrfeed"
_VERSION = "2024011101"

_VERBOSE = False


class Main:
    def __init__(self, verbose=False):
        self.appname = _APPNAME
        self.version = _VERSION

        self.verbose = verbose

        self.log_path = pdirs.user_log_path()
        self.config_path = pdirs.user_config_dir(_APPNAME)
        self.config_path = "NOINCLUDE"  # TODO: REMOVE THIS LINE
        self.cache_path = pdirs.user_cache_dir(_APPNAME)
        self.data_raw_path = pdirs.user_data_dir(_APPNAME) + "/mail_raw"
        self.data_pdf_path = pdirs.user_data_dir(_APPNAME) + "/mail_pdf"

        self.download_path = self.data_raw_path  # TODO REMOVE DOWNLOAD_PATH!!

        self.cfg = configparser.ConfigParser()

        self._create_paths()  # TODO: Only if needed, called by modules!

    def _create_paths(self):
        # TODO Create missing PATHs only depending on module's needs!
        for path in [self.download_path, self.config_path, self.cache_path, self.data_raw_path, self.data_pdf_path]:
            if os.path.isdir(path):
                if self.verbose:
                    print(f"[x] Path '{path}' exists.")
            else:
                try:
                    os.makedirs(path)
                except:
                    print(f"[ ] PATH '{path}' could not be created!")
                else:
                    print(f"[x] Path '{path}' has been created.")

    # NEW STRUCTURE

    def load_config(self):

        config_file = self.config_path + "/config.toml"

        # load config
        # [ ] TODO: Switch to proper path / add config file template copying to config path
        if os.path.isfile(config_file):
            # return config.Config(config_file)
            try:
                self.cfg.read(config_file)
            except:
                print(f"Config file '{config_file}' could not be loaded.")
                self.cfg = None
        else:
            try:
                with open("templates/config.toml", "r") as infile:
                    with open(config_file, "w") as outfile:
                        outfile.writelines(infile.readlines())
            except:
                print(f"ERROR: Missing {type} config file.")
                print("       A config file template has been created at")
                print(f"       {config_file}")
            finally:
                self.cfg = None

    def run(self):
        self.load_config()
        if self.cfg is None:
            print(f"No valid configuration found in {self.config_path}.")
            print("Quit.")
            exit()

        # Run Modules depending on configuration.
        # This can be used to split up Papyrfeed's functionality between multiple hosts.
        if self.cfg["imap"]["enable"] == "True":
            if self.verbose:
                print("Running IMAP fetch")
            self.fetch_from_imap()
        if self.cfg["dirs"]["enable"] == "True":
            self.fetch_from_dirs()
        if self.cfg["convert"]["enable"] == "True":
            self.convert()
        if self.cfg["smtp"]["enable"] == "True":
            pass
            # TODO: Expand config file for reporting levels,
            #       implement a report collection to push it in one mail, etc.
        if self.cfg["import"]["enable"] == "True":
            self.import_to_paperwork()

    def fetch_from_imap(self):
        if self.verbose:
            print("Fetching mail.")
        imap = self.cfg["imap"]
        for o in imap:
            print(imap[o])
        # get list of email bodies from INBOX folder
        try:
            with imap_tools.MailBox(imap["host"]).login(imap["login"], imap["passwd"], imap["folder"]) as mailbox:

                if 'OK' in mailbox.login_result:
                    print(25 * "_" + " fetching mail " + 25 * "_")
                    for msg in mailbox.fetch():
                        # [ ] TODO: Log this:
                        print(f"{msg.uid} === {msg.date} === '{msg.subject}'  FROM {msg.from_}")

                        # [ ] TODO: if self.verbose:
                        if self.verbose:
                            print("--------html-body------")
                            print(msg.html)
                            print("--------text-body------")
                            print(msg.text)
                            print("-----------------------")

                        # save all attachments in binary format to a file with its name containing original name, date, sender
                        # TODO: This is the "auto" setting.
                        if len(msg.attachments) > 0:
                            print(" ->", len(msg.attachments), "Attachments found")
                            for att in msg.attachments:
                                print(f"    - {att.filename} ({att.content_type})")
                                # save to file
                                with open(f"{self.download_path}/{msg.from_}-{msg.date}-{msg.subject}-{att.filename}", "wb") as f:
                                    f.write(att.payload)
                        else:
                            if len(msg.html) > 0:  # HTML body is preferred over txt body
                                print(" -> HTML body found")
                                # save to file
                                with open(f"{self.download_path}/{msg.from_}-{msg.date}-{msg.subject}-body.html", "w") as f:
                                    f.write(msg.html)
                            elif len(msg.text) > 0:  # txt body is ignored if HTML body is present!
                                print(" -> TEXT body found")
                                # save to file
                                with open(f"{self.download_path}/{msg.from_}-{msg.date}-{msg.subject}-body.txt", "w") as f:
                                    f.write(msg.text)
        except:
            print(f"ERROR: Mail could not be fetched from host {imap['host']}.\nCheck configuration.")

                # Now delete the processed E-Mail by uid:
                # [ ] TODO: Processed mails should be moved to a "processed" folder!
                #           - Check if folder exists, else create.
                # [ ] TODO: If "processed" folder is huge, ask for purging before importing new mails.
                #           - Present "older than" selector buttons (1 day, 1 week, 1 month, 1 year)
                # To delete mails: Switch to folder, search and delete uids
                # mailbox.move(uid, "PROCESSED")
                # Purging of mails: Switch to folder, search and delete uids
                # mailbox.folder("PROCESSED")
                # mailbox.search(........)
                # mailbox.delete(uid)

    def fetch_from_dirs(self):
        if self.verbose:
            print("Fetching files from directories.")
        for p in self.cfg["dirs.paths"]:
            if os.path.isdir(self.cfg["dirs.paths"][p]):
                print(f"Path {self.cfg['dirs.paths'][p]} exists.")
                # TODO: Copy file with a proper filename to "download path"
            else:
                print(f"Path {self.cfg['dirs.paths'][p]} does NOT exist.")

    def convert(self):
        if self.verbose:
            print("Converting files.")
        for filename in os.listdir(self.data_raw_path):
            infile = f'{self.data_raw_path}/{filename}'
            print(f'file: {infile}')
            try:
                conv_args = ['unoconv', '--preserve', '--format=pdf', f'--output={self.data_pdf_path}/{filename}.pdf', infile]
                output = subprocess.check_output(conv_args)
            except subprocess.CalledProcessError as e:
                output_decoded = e.output.decode('utf-8')
                print("unoconv exception raised! e.output:\n", output_decoded)
                if output_decoded.startswith('error: {'):
                    error = json.loads(output_decoded[7:])  # Skip "error: "
                    print(error['code'])
                    print(error['message'])
                else:
                    print(f"unoconv produced an error!\n{output_decoded}")
                    print(f"file <{infile} could not be converted to PDF.")
                    # TODO raise an internal flag?
            else:
                print("unoconv output:\n", output)
            finally:
                pass

            # ALTERNATIVES:
            # VER 1 (withOUT output)
            # subprocess.call(['ls', '-a'])
            # VER 3 (with output)
            # subprocess.check_call(['ls', '-a'])

            # This could also be run in the bash wrapper(!)

    def import_to_paperwork(self):
        pass


# TODO Find out how to send mails (error reports etc.)
    # could be done via himalaya!
    # It would be nicer to run this via "mailbox" from pythong, but...
    # This would mean to setup both, papyrfree AND himalaya, but if it works for me...


if __name__ == '__main__':
    print(f"====== PAPYRFREE ====== v{_VERSION}")
    args = sys.argv
    for arg in args:
        _VERBOSE = arg in ['-v', '--verbose']
        if arg in ['-h', '--help']:
            print('-v --verbose')
            print('-h --help')

    # main_imapclient()
    app = Main(verbose=_VERBOSE)

    # app.run_old()
    app.run()
